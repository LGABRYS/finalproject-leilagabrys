# FinalProject-LeilaGabrys

## Name
2014 Hurricane Forecast and Observation Verification

## Description
The goal of this project is to verify and compare observed vs forecasted data of tropical cyclones in the Atlantic and Eastern Pacific in the year 2014. It will read in a set of over 1000 emails with the forecasted data, as well as 30 BestTrack.dat files for the same storms' observed data. This python script is meant to be ran all at once, so as not to append anything to a list twice. Re-running a cell will not handle data correctly. My SSEC work is done in a text editor, but I have added the work to JupyterHub for ease. 

This project is much of a data wrangling project and requires some different things to make it the most efficient. Since I was most versed in dictionaries, I aimed to use those the most, but then also use them to build on my pandas and dataframe knowledge. Overall, most of the data verified in the graphs, despite the limitations of the dataset.

## Installation
Email Files: courtesy of CIMSS Tropical Cyclone Group

To download the BestTrack data: NHC Archive.
To match with the email data, we need to download all of the 2014 emails with the data that start with "bal" (for Atlantic Ocean) and "bep" (for eastern Pacific).

To unzip these files, navigate to your directory and type in the terminal: gunzip *.gz

## Support
Email lgabrys@wisc.edu for support.

## Roadmap
It would be interesting to do an expansion and add units using xarray to these data sets to provide more clarity on the data we're working with. 

## Authors and acknowledgment
Thank you to the CIMSS Tropical Cyclone Group for access to the data. Thank you to Professor Hannah Zanowski and Cameron Bertossa for their help with lectures in AOS 573 this fall. 

## Project status
This was the farthest extension on my research project that was successful (these graphs). Most of the data from the emails did line up with the observed data from the BestTrack files. The graphs did show that there were some limitations to both of the datasets. Some storms had fewer durations of storm data taken, and so the graphs look wonky and incomplete (seen in storm 20E, there were only three email pressure points, and some besttrack files only showed having one measurement over the span of the storm, seen in 02L). Some datasets had jumps where no data could be taken (seen in the email data, such as storm 04E). The next part of my project that I got started on after this was looking at the forecast intervals (0hr to 24hr) showing how favorable or unfavorable the environment is for tropical cyclone intensification (which were not graphed above). I got a start on visualizing it at the end of my project.
